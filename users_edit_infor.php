<?php
$pageTitle = "Modifier mes infors personnelles";
require_once('src/controllers/users_edit_infor.php');
ob_start();
$utilisateur = getDetailUtilisateur($id_utilisateur);
?>
  <div class="title">
    <h1 class="text-center pt-5 mb-5" id="utilisateur_modifi_infor_h1"> MODIFIER MES INFORMATIONS </h1>
  </div>
  <div class="container mt-5 d-flex justify-content-center">
    <div class="utilisateur_modifi_box mt-5">
      <?php if(isset($msgError)){  ?>
        <div class="my-3 text-center">
            <a href="#" class="alert alert-danger" style="text-decoration: none;"><?php echo $msgError ?></a>
        </div>
      <?php }  ?>    
      <form action="users_edit_infor.php?id_utilisateur=<?php echo $utilisateur['id'] ?>" method="POST">
            <div class=" mb-4 text-center">
                <label for="name">Nom * :</label>
                <input  type="text" class="form-control mt-2 text-center" id="nom" name="nom" value="<?php echo $utilisateur['nom'] ?>">
            </div>
            <div class=" mb-4 text-center">
                  <label for="web_site">Prénom * :</label>
                  <input type="text" class="form-control mt-2 text-center" id="prenom" name="prenom" value="<?php echo $utilisateur['prenom'] ?>">
            </div>
            <div class=" mb-4 text-center">
                  <label for="web_site">Mail * :</label>
                  <input type="text" class="form-control mt-2 text-center" id="mail" name="mail" value="<?php echo $utilisateur['mail'] ?>">
            </div>
            <div class="mt-3 text-center">
              <a href="./users_edit.php" class="btn mt-3 px-5 mx-3">ANNULER</a>
              <button type="submit" name="modifier" class="btn mt-3 px-5"> MODIFIER </button>
            </div>
      </form>
    </div>
  </div>




<?php
$content = ob_get_clean();
require_once('templates/layout.php');

?>