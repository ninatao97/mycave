<?php
$pageTitle = "Page d'acceuil";
require_once('./src/models/products.php');

$produits = getProduitsHome();
ob_start();
?>

<div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/front.jpg" class="d-block w-100" alt="...">
    </div>
  </div>
</div>
<div class="mt-5 mb-5 text-center container intro_site">
  <h2 class="text-center"> Introducation du site </h2>
  <p class="text-center intro_entreprise mt-5" >Lorem ipsum dolor sit, amet consectetur adipisicing elit. Non ducimus, sed aliquid, nesciunt obcaecati, totam sint id quas fuga debitis officia eaque minus nostrum! Voluptatum reiciendis cumque in quam libero!
  Adipisci placeat doloribus veniam animi. Velit maiores ipsam reiciendis, rerum, id eveniet natus exercitationem non et obcaecati labore? Iusto labore soluta quidem, at mollitia eos fugit similique corrupti consequatur reiciendis.
  Eius vel nemo modi nam omnis aperiam recusandae dolorem quod quasi quidem possimus iure aut, voluptates corrupti inventore. A temporibus deserunt dolore sequi sint accusantium voluptatum provident fugit voluptate totam.
  Maxime sit dolores porro voluptates facere, eaque possimus ipsum vel at aut. Animi doloribus ducimus dolorem aspernatur voluptas accusantium adipisci quod officia quis, modi vel iure unde enim laudantium recusandae.
  Debitis blanditiis ex optio quam? Deserunt at perferendis ratione a sit? Alias voluptatibus molestiae tenetur repellat ipsum id quibusdam fugit consequuntur quam? Deleniti, alias libero pariatur vero quae facere! Aliquid?</p>
</div>
<div class="show_produits my-5 slide_produit">
  <h2 class="text-center my-5">Nos produits</h2>
  <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <p class="text-center">Cliquez pour voir nos produits</p>
    </div>
<?php foreach($produits as $produit){ ?>
    <div class="carousel-item">
    <div class="card mb-5 p-3 card_produit" style="max-width: 540px; overflow:hidden; margin:auto;">
            <div class="row">
              <div class="col-md-4" style="margin: auto;">
                <img class="j-center" style="position: relative; left: 50%; transform: translateX(-50%);" src="./img/<?php echo $produit['picture'] ?>" alt="Photo du produit" style="margin: auto;">
              </div>
              <div class="col-md-8 p-3">
                <div class="card-body produit_box">
                  <h5 class="card-title text-center"><?php echo $produit['domaine'] ?></h5>
                  <div class="justify-content-center d-flex">
                    <span class="mx-3"><i class="far fa-thumbs-up"></i>(<?php echo $produit['likes'] ?>)</a>
                    <span class="mx-3"><i class="far fa-thumbs-down"></i>(<?php echo $produit['dislikes'] ?>)</a>
                  </div>
                  <ul class="list-group mt-3 list-group-flush">
                      <li class="list-group-item text-center">Pays : <?php echo $produit['country'] ?></li>
                  </ul>
                  <div class="d-flex justify-content-center mb-3 mt-5">
                      <a href="./product_detail.php?id_produit=<?php echo $produit['id'] ?>" class="btn"> PLUS D'INFORMATION </a>
                   </div>                   
                </div>
              </div>
            </div>
            
          </div>    </div>
<?php } ?>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
</div>

<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>
