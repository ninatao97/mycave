<?php
require_once('src/controllers/product_edit.php');
 $pageTitle = "Modification de produit";
 ob_start();
?>


<div class="title">
  <h1 class="text-center pt-5 mb-5" id="produit_modifi_h1"> MODIFIER UN ELEMENT </h1>
</div>
<div class="container d-flex justify-content-center">
    <div class="produit_modifi_box">
      <?php if(isset($msgError)){  ?>
      <div class="my-5 text-center">
            <a href="#" class="alert alert-danger" style="text-decoration: none;"><?php echo $msgError ?></a>
      </div>
      <?php }  ?>    
      <form action="product_edit.php?id_produit=<?php echo $produit['id'] ?>" method="POST" enctype="multipart/form-data" class="d-flex flex-column justify-content-center">
        <div class="mb-3 text-center">
            <label for="domaine">DOMAINE * :</label>
            <input type="text" class="form-control my-2 text-center" id="domaine" name="domaine" value="<?php echo $produit['domaine'] ?>" >
      </div>
      <div class="mb-3 text-center">
            <label for="country">Pays * :</label>
            <input type="text" class="form-control my-2 text-center" id="country" name="country" value="<?php echo $produit['country'] ?>">
      </div>
      <div class="mb-3 text-center">
            <label for="region">Région * :</label>
            <input type="text" class="form-control my-2 text-center" id="region" name="region" value="<?php echo $produit['region'] ?>">
      </div>
      <div class="mb-3 text-center">
            <label for="year">Année * :</label>
            <input type="text" class="form-control my-2 text-center" id="year" name="year" value="<?php echo $produit['year'] ?>">
      </div>
      <div class="mb-3 text-center">
            <label for="grape">Grape * :</label>
            <input type="text" class="form-control my-2 text-center" id="grape" name="grape" value="<?php echo $produit['grape'] ?>">
      </div>
      <div class="mb-3 text-center">
            <label for="picture" class="form-label"> Séléctionner un ficher *</label>
            <input class="form-control" type="file" id="picture" name="picture">
      </div>
      <div class="mt-3 text-center">
            <label for="description">Description</label>
            <textarea class="form-control my-2 text-center" id="description" name="description" style="height: 200px; width: 500px; resize:none;"><?php echo $produit['description'] ?></textarea>
      </div>
      <div class="mt-3 text-center">
            <button type="submit" name="modifier" class="btn mt-5 px-5" > MODIFIER </button>
      </div>

      </form>
    </div>
</div>


<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>