<?php
require 'crypte.php';
require_once('src/models/users.php');

$id_utilisateur = $_GET['id_utilisateur'];
$utilisateur = getDetailUtilisateur($id_utilisateur);
$act_mail = $utilisateur['mail'];
// var_dump($act_mail);
// die;

if(isset($_POST['modifier'])){
    $new_mail = checkInput($_POST['mail']);
    if(!filter_var($new_mail, FILTER_VALIDATE_EMAIL)){
        $msgError = "Address mail non valide! ";
    }else{
        //Si c'est la même
        if($new_mail==$act_mail){
            $mail = $act_mail;
        }else{
        //Si c'est différent
            $res = checkMailUtilisateur($new_mail);
            if(is_array($res)){
               $msgError = "Ce mail est déjà inscrit!";
               return $msgError;               
            }
            $mail = $new_mail;
        }
        $newUtilisateurValues = [
            'nom' => checkInput($_POST['nom']) ,
            'prenom' => checkInput($_POST['prenom']),
            'mail' => $mail
        ];
        
        $res2 = modifiUtilisateurInfor($id_utilisateur,$newUtilisateurValues);
        if($res2){
           /* $res3 = getNewUtilisateurInfor($id_utilisateur);
            $_SESSION['utilisateur'] = $res3;*/
            header('Location: ./users_edit.php');
            $msgError = "Modification validée!";
            exit;
            
        }

        
    }
}
?>