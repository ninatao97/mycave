<?php
session_start();
require_once('../../src/models/products.php');

$id_produit = $_GET['id_produit'];
$id_utilisateur = $_SESSION['utilisateur']['id'];

if(isset($_GET['t'],$_GET['id_produit']) && !empty($_GET['t']) && !empty($_GET['id_produit'])){
    //vérifier l'existance d'id de produit
    $check = getDetailProduit($id_produit)['id'];
    $getAction = (int)$_GET['t'];
    if(!empty($check)){
        $checkUserLike = checkLike($id_utilisateur,$id_produit);
        $checkUserDislike = checkDislike($id_utilisateur,$id_produit);
        if($getAction == 1){
            //Partie : LIKE
            //Pas encore mettre like
            if(!is_array($checkUserLike)){
                $add_like = addLike($id_produit);
                $update_infor = updateAddLikeInfor($id_produit,$id_utilisateur);
                $checkUserDislike = checkDislike($id_utilisateur,$id_produit);
                //Si l'utilisateur clique sur LIKE il faut enlever son DISLIKE avant
                if(is_array($checkUserDislike)){
                    $delete_Dislike = deleteDisLike($id_produit);
                    $update_infor = updateDeleteDislikeInfor($id_produit,$id_utilisateur);    
                }
            }else{
                //Déjà mettre like
                $deleteLike = deleteLike($id_produit);
                $update_infor = updateDeleteLikeInfor($id_produit,$id_utilisateur);
            }
        }elseif($getAction == 2){
            //Partie : DISLIKE
            //Pas encore mettre dislike
            if(!is_array($checkUserDislike)){
                $add_dislike = addDislike($id_produit);
                $update_infor = updateAddDislikeInfor($id_produit,$id_utilisateur);
                //Si l'utilisateur clique sur DISLIKE il faut enlever son LIKE avant
                if(is_array($checkUserLike)){
                    $deleteLike = deleteLike($id_produit);
                    $update_infor = updateDeleteLikeInfor($id_produit,$id_utilisateur);    
                }
            }else{
                //Déjà mettre dislike
                $delete_Dislike = deleteDisLike($id_produit);
                $update_infor = updateDeleteDislikeInfor($id_produit,$id_utilisateur);
            }
        }
        header('Location: http://mycave/product_detail.php?id_produit='.$id_produit);
    }else{
        exit("Erreur !");
    }

}else{
    header('Location: ./product_list.php');
}