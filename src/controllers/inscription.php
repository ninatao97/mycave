<?php
require 'crypte.php';
require_once('src/models/users.php');

session_start();
if(isset($_POST['valider'])){
    //Vérifier si l'input-mail est en forme email
    if(!filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)){
        $msgError = "Address mail non valide! ";
    }else{
        //checkInput() une fonction crypté dans crype.php
        $res = checkMailUtilisateur(checkInput($_POST['mail']));
        if($res){
            $msgError = "Le compte exite déjà !";
        }else{
            $newUtilisateurValues = [
                'nom' => checkInput($_POST['nom']),
                'prenom' => checkInput($_POST['prenom']),
                'mail' => checkInput($_POST['mail']),//condition: filter_var($var,param_email....)
                'password' => cryptePassword(checkInput($_POST['password'])),//pass_hash pour envoye ret retourner
                'id_role' => $_POST['id_role']
            ];
            $res2 = addUtilisateur($newUtilisateurValues);
            /** AFIN DE SE CONNECTER APRES L4INSCRIPTION
             * ICI le ligne 5: session_start() il ne faut pas oublier
             * on utiliser $_SESSION ain d'enreigistrer les infors de nouveaux utilisateurs
             */
            $login = loginUtilisateur($newUtilisateurValues['mail'],$newUtilisateurValues['password']);
            $_SESSION['utilisateur'] = $login;
            $_SESSION['msgReussite'] = 'Bienvenue';
            header('Location: ./page_all_products.php');
            exit;
       }
    }
    
    
    
}
    


?>