<?php
require 'crypte.php';
require_once('src/models/products.php');

$searchValues = checkInput($_GET['search']);

if($searchValues !== ''){
    $produits = searchProduit($searchValues);
    if($produits == false){
        $msgError = "AUCUN RESULTAT TROUVE !";
    }
}else{
    $msgError = "LA VALEUR DE RECHERCHE EST VIDE !";
}

?>