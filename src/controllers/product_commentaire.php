<?php
session_start();
require 'crypte.php';
require_once('src/models/products.php');

$id_produit = $_GET['id_produit'];
$id_utilisateur = $_SESSION['utilisateur']['id'];

if($check = getDetailProduit($id_produit)['id']){
    $commentaires = getCommentaires($id_produit);
    if(isset($_POST['valider'])){
        $commentaire = checkInput($_POST['commentaire']);
        $res = addCommentaire($id_produit,$id_utilisateur,$commentaire);
        if($res){
            $msgErreur = "Votre commentaire est bien enregistré";
            header('Location:http://mycave/product_detail.php?id_produit='.$id_produit);
            exit;
        }
    }
}