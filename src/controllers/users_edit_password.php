<?php
require 'crypte.php';

require_once('src/models/users.php');
$id_utilisateur = $_GET['id_utilisateur'];

//bouton submit
 if(isset($_POST['modifier'])){
   /**
    * Etapes pour modifier le MDP de l'utilisateur
    *  - D'abord il faut obtenir le MDP actuel dans mySQL
    *  - 2ème, $_POST tous les champs entrés
    *  - 3ème ajouter des conditions avant la modification de MDP
    *        --- 1.)si les 2 nouveaux MDP ne sont pas identiques, $msgError
    *        --- 2.)si MDP actuel demandé != MDP actuel SQL , $msgError
    *        --- 3.)si le nouveau MDP == MDP actuel SQL , $msgError
    *  --- Si ne rentre pas dans les condition au-dessus, il va prendre la requête afin de modifier le MDP
    ***
    */
    $id_utilisateur = $_GET['id_utilisateur'];
    $password_actuel = getUtilisateurMDP($id_utilisateur);

    $enter_password_actuel = cryptePassword(checkInput($_POST['enter_password_actuel']));
    $new_password          = cryptePassword(checkInput($_POST['new_password']));
    $confirm_new_password  = cryptePassword(checkInput($_POST['confirm_new_password']));



    if($new_password != $confirm_new_password){
        $msgError = "Les 2 nouveaux mots de passe ne sont pas identiques !";
    }elseif($enter_password_actuel != $password_actuel){
        $msgError = "Le mot de passe actuel entré est incorrect !"; 
    }elseif($new_password == $password_actuel){
        $msgError = "Le nouveau mot de passe doit être différent de l'ancien !";
    }else{
        $res = modifiUtilisateurMDP($id_utilisateur,$new_password);
        if($res){
                $_SESSION['msgReussite'] = "Modification validée!";
                header('Location: ./users_edit.php');
                exit;
        }
        
    }
 

 }


?>