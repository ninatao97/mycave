<?php
require 'crypte.php';
require_once('src/models/users.php');
session_start();
//Si l'utilisateur s'est déjà connecté, ne prend pas les étapes suivantes
// saute directement à la page index.php
if(isset($_SESSION['utilisateur'])){
    header('Location: ./page_all_products.php');
    exit;
}

if(isset($_POST['connect'])){
    $mail = checkInput($_POST['mail']);
    $pwd_crypte = cryptePassword(checkInput($_POST['password']));
    
    
    $res = loginUtilisateur($mail,$pwd_crypte);
    if($res){
        //Si l'utilisateur a réussi de se connecter, 
        //on enregistrer ses information de connexion 
        // mail et mot de passe
        //et aussi tous les informations dans la fonction loginUtilisateur
        $_SESSION['utilisateur'] = $res;
        $_SESSION['msgReussite'] = 'Bienvenue';
        header('Location: ./page_all_products.php');
        exit;
    }else{
        $msgError = "Votre mot de passe ou votre email semble erroné";
    }
}
?>