<?php
require 'crypte.php';
require_once('src/models/products.php');
$format_allow = array("image/jpeg", "image/jpg", "image/png");
/**
         * if(!isset($_FILES['picture']) && !is_array($FILES['picture']))
         * if upload est vide ---> $picture = 'default.jpg';
         * else: upload n'est pas vide
         *  1.verifier le format
         *  2.verifier le poid
         *  3.random le nom de fichier dans le bdd
*/

if(isset($_POST['creer'])){
    $format_picture = $_FILES['picture']['type'];
    $year = checkInput($_POST['year']);
    if(strlen($year) != 4 || !is_numeric($year)){
        $msgError ="Ce format de date n'est pas accepté";
    }elseif($year > 2021){
        $msgError ="Ce format d'année n'est pas accepté";
    }else{
        if($_FILES['picture']['error'] == 4){
            $picture = 'default.jpg';
        }elseif(!in_array($format_picture,$format_allow)){
            $msgError = "Vérifiez que le fichier à importer est au format JPG,JPEG ou PNG";
        }elseif($_FILES["picture"]["size"] > 20480){
            $msgError = "Le poid maximum du fichier à importer doit être moins de 20Kb"; 
        }else{
            $picture = $_FILES['picture'];
            $picture = rand(0,100000000) .$_FILES['picture']['name'];
        }
        $produitValues = [
            'domaine' => checkInput($_POST['domaine']), // htmlentities($var, ENT_QUOT);
            'year' => $year,
            'grape' => checkInput($_POST['grape']),
            'country' => checkInput($_POST['country']),
            'region' => checkInput($_POST['region']),
            'description' => checkInput($_POST['description']),
            'picture' => $picture
        ];
    
        $res = addProduit($produitValues);
        if($_FILES['picture']['name'] != ''){
            move_uploaded_file($_FILES['picture']['tmp_name'], './img/'.$picture);
        }
    
        if($res){
            $_SESSION['msgReussite'] = 'Produit ajouté!!';
            header('Location: ./product_list.php');
            exit;
        }
        
    }
}


?>

