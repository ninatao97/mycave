<?php
require 'crypte.php';
require_once('src/models/products.php');

$id_produit = $_GET['id_produit'];
$produit = getDetailProduit($id_produit);
$format_allow = array("image/jpeg", "image/jpg", "image/png");

if(isset($_POST['modifier'])){
    $format_picture = $_FILES['picture']['type'];
    $year = checkInput($_POST['year']);
    if(strlen($year) != 4 || !is_int($year)){
        $msgError ="Ce format de date n'est pas accepté";
    }elseif($year > date('Y')){
        $msgError ="Ce format d'année n'est pas accepté";
    }else{
        if($_FILES['picture']['error'] == 4){
            $picture = $produit['picture'];
        }elseif(!in_array($format_picture,$format_allow)){
            $msgError = "Vérifiez que le fichier à importer est au format JPG,JPEG ou PNG";
        }elseif($_FILES["picture"]["size"] > 20480){
            $msgError = "Le poid maximum du fichier à importer doit être moins de 20Kb"; 
        }else{
            $picture = $_FILES['picture'];
            $picture = rand(0,100000000) .$_FILES['picture']['name'];
            //$picture = uniqid() .$_FILES['picture']['name'];
        }
        $updateValues = [
            'domaine' => checkInput($_POST['domaine']), 
            'year' => $year,
            'grape' => checkInput($_POST['grape']),
            'country' => checkInput($_POST['country']),
            'region' => checkInput($_POST['region']),
            'description' => checkInput($_POST['description']),
            'picture'=> $picture
        ];
        $res = updateProduit($id_produit, $updateValues);
        if($res){
                if($_FILES['picture']['name']!=''){
                    move_uploaded_file($_FILES['picture']['tmp_name'], './img/'.$picture);
                    if($produit['picture']!='default.jpg'){
                        unlink('./img/'.$produit['picture']);
                    }
                }
                $_SESSION['msgReussite'] = "Modification réussite !!";
                header('Location: ./product_list.php');
                exit;
        }
        
    }
    
}

?>