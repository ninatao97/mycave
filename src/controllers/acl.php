<?php
    $phpSelf = $_SERVER['PHP_SELF'];
    $currentPage = pathinfo(basename($phpSelf))['filename'];

    $tabPageAdmin = [
        //10: 
        'product_edit' => 50,
        'product_add' => 50,
        'product_delete' => 50,
        'product_detail' => 10,
        'product_list' => 50,
        'product_list_search' => 50,
        'users_list' => 100,
        'users_detail' => 100,
        'users_delete' => 100,
        'users_edit_infor' => 10,
        'users_edit_password' => 10,
    ];
    $res = isset($tabPageAdmin[$currentPage]);
    if($res === true &&(!isset($_SESSION['utilisateur']) || $_SESSION['utilisateur']['poid']<$tabPageAdmin[$currentPage])){
        //echo "Vous ne pouvez pas accéder à cette page";
        header('Location: ./login.php');
        exit;
    }
?>