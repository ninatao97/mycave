<?php
function u_getPDOConnect(){
    require('connect.php');
    return $pdo;
}

function addUtilisateur($newUtilisateurValues){
    $pdo = u_getPDOConnect();

    $stmt = $pdo->prepare("INSERT INTO `users`(nom, prenom, mail, password, id_role ) 
    VALUES (:nom,:prenom,:mail,:password,:id_role)");


    $stmt -> bindParam(':nom',$newUtilisateurValues['nom']);
    $stmt -> bindParam(':prenom',$newUtilisateurValues['prenom']);
    $stmt -> bindParam(':mail',$newUtilisateurValues['mail']);
    $stmt -> bindParam(':password',$newUtilisateurValues['password']);
    $stmt -> bindParam(':id_role',$newUtilisateurValues['id_role']);

    $res = $stmt->execute();

    if(!$res){
        return false;
    }
    return true;
}


function loginUtilisateur($mail,$pwd_crypte){
    $pdo = u_getPDOConnect();

    $stmt = $pdo->prepare("SELECT
        u.id,
        u.nom,
        u.prenom,
        u.mail,
        u.id_role,
        r.name,
        r.poid
    FROM
        users u
    JOIN roles r ON u.id_role = r.id
    WHERE
        `mail`=:mail AND `password`=:password");

    $stmt->bindParam(':mail',$mail);
    $stmt->bindParam(':password',$pwd_crypte);

    $res = $stmt->execute();
    if($res){
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}


function getUtilisateursList(){
   try{ 
    $pdo = u_getPDOConnect();

    return $pdo->query("SELECT
        u.id,
        u.nom,
        u.mail,
        u.id_role,
        r.name
    FROM
        users u
    JOIN roles r ON u.id_role = r.id
    ORDER BY `create_time`
    DESC",PDO::FETCH_ASSOC);
   }catch(\PDOException $error){
        return false;
    }
}



function getDetailUtilisateur($id_utilisateur){
    $pdo = u_getPDOConnect();

    $stmt = $pdo->prepare("SELECT u.id, u.nom, u.prenom,
     u.mail, u.id_role, u.update_time, r.name 
    FROM users u 
    JOIN roles r ON u.id_role = r.id 
    WHERE u.id=:id");
    
    $stmt->bindParam(':id',$id_utilisateur);
    $res = $stmt->execute();
    if($res){
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}


function deleteUtilisateur($id_utilisateur){
    $pdo = u_getPDOConnect();

    $stmt = $pdo->prepare("DELETE FROM `users` WHERE `id`=:id");
    $stmt->bindParam(':id',$id_utilisateur);

    $res = $stmt->execute();
    
    return $res;
}

function checkMailUtilisateur($mail){
    $pdo = u_getPDOConnect();
    $stmt = $pdo->prepare("SELECT `mail`
    FROM `users` 
    WHERE `mail`=:mail");

    $stmt ->bindParam(':mail',$mail);

    if($stmt->execute()){
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}

function modifiUtilisateurInfor($id_utilisateur,$newUtilisateurValues){
    $pdo = u_getPDOConnect();

    $stmt = $pdo->prepare("UPDATE `users` 
    SET `nom`=:nom,`prenom`=:prenom,`mail`=:mail 
    WHERE `id`=:id");
    $stmt->bindParam(':id',$id_utilisateur);
    $stmt->bindParam(':nom',$newUtilisateurValues['nom']);
    $stmt->bindParam(':prenom',$newUtilisateurValues['prenom']);
    $stmt->bindParam(':mail',$newUtilisateurValues['mail']);

    $res = $stmt->execute();
    if($res){
        return true;
    }else{
        return false;
    }
}




function getUtilisateurMDP($id_utilisateur){
    $pdo= u_getPDOConnect();

    $stmt = $pdo->prepare("SELECT `password` 
    FROM `users` 
    WHERE `id`=:id");
    $stmt->bindParam(':id',$id_utilisateur);
    //$res est pour voir le requête si ça marche
    $res = $stmt->execute();
    if($res){
        //$userMDP est pour voir ça retourner le quelque chose
        $userMDP = $stmt->fetch(PDO::FETCH_ASSOC);
        if($userMDP){
            //si ça retourne le MDP, return le MDP
            return $userMDP['password'];
        }else{
            //Sinon return false
            return false;
        }
    }else{
        return false;
    }
}


function modifiUtilisateurMDP($id_utilisateur,$newPassword){
    $pdo = u_getPDOConnect();
    $stmt = $pdo->prepare("UPDATE `users` 
    SET `password`=:password 
    WHERE `id`=:id");
    $stmt->bindParam(':id',$id_utilisateur);
    $stmt->bindParam(':password',$newPassword);

    $res = $stmt->execute();
    if($res){
        return true;
    }else{
        return false;
    }
}

function searchUtilisateur($searchValues){
    $pdo = u_getPDOConnect();

    $stmt = $pdo->prepare("SELECT
    	u.id,
        u.nom,
        u.mail,
        u.id_role,
        r.name
    FROM
        users u
    JOIN roles r ON u.id_role = r.id
    WHERE r.name = :search
    GROUP BY u.id
    ORDER BY `create_time`
    DESC");
    $stmt->bindParam(':search',$searchValues);
    $res =$stmt->execute();
    if($res){
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}




?>