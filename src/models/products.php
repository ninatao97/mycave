<?php
function getPDOConnect(){
    require('connect.php');
    return $pdo;
}

function getProduitsHome(){
    try{
        $pdo =getPDOConnect();
        return $pdo->query("SELECT `id`, `domaine`, `year`, `grape`, `country`, `picture`,`likes`,`dislikes`  
        FROM `products` 
        ORDER BY `create_time`
        DESC",PDO::FETCH_ASSOC);

    }catch(\PDOException $error){
        return false;
    }
}


function getDetailProduit($id_produit){
    $pdo = getPDOConnect();
    //ici c'est très important de SELECT id aussi car sinon il ne peut pas capturer le id de produit afin d'afficher des inofrmation
    $stmt = $pdo->prepare("SELECT `id`,`domaine`, `year`, 
    `grape`, `country`, `region`, `description`, 
    `picture`,`update_time`,`likes`,`dislikes`
    FROM `products` 
    WHERE 
    `id`=:id");
    
    $stmt->bindParam(':id',$id_produit);
    $res = $stmt->execute();
    if($res){ 
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}


function deleteProduit($id_produit){
    $pdo = getPDOConnect();

    $stmt = $pdo->prepare("DELETE FROM `products` WHERE `id`=:id");
    $stmt ->bindParam(':id',$id_produit);

    $res = $stmt->execute();
    return $res;
}



function getProduitsList(){
    try{
        $pdo = getPDOConnect();
        return $pdo->query('SELECT `id`, `domaine`, `year`, `country`, `picture`,`likes`,`dislikes`  
        FROM `products` 
        ORDER BY `create_time` DESC');

    }catch(\PDOException $error){
        return false;
    }
}

function addProduit($produitValues){
    $pdo = getPDOConnect();

    $stmt = $pdo->prepare("INSERT INTO `products`(`domaine`, `year`, `grape`, `country`, 
    `region`, `description`, `picture`) 
    VALUES (:domaine,:year,:grape,:country,:region,:description,:picture)");

    $stmt->bindParam(':domaine',$produitValues['domaine']);
    $stmt->bindParam(':year',$produitValues['year']);
    $stmt->bindParam(':grape',$produitValues['grape']);
    $stmt->bindParam(':country',$produitValues['country']);
    $stmt->bindParam(':region',$produitValues['region']);
    $stmt->bindParam(':description',$produitValues['description']);
    $stmt->bindParam(':picture',$produitValues['picture']);

    $res = $stmt->execute();

    if($res){
        return $res;
    }else{
        return false;
    }
}


function updateProduit($id_produit, $updateValues){
    $pdo = getPDOConnect();

    $stmt = $pdo->prepare("UPDATE `products` 
    SET `domaine`=:domaine,`year`=:year,
    `grape`=:grape,`country`=:country,
    `region`=:region,`description`=:description,
    `picture`=:picture
    WHERE `id`=:id");

    $stmt ->bindParam(':id',$id_produit);
    $stmt ->bindParam(':domaine',$updateValues['domaine']);
    $stmt ->bindParam(':year',$updateValues['year']);
    $stmt ->bindParam(':grape',$updateValues['grape']);
    $stmt ->bindParam(':country',$updateValues['country']);
    $stmt ->bindParam(':region',$updateValues['region']);
    $stmt ->bindParam(':description',$updateValues['description']);
    $stmt ->bindParam(':picture',$updateValues['picture']);

    $res = $stmt->execute();
    if($res){
        return true;
    }else{
        return false;
    }
}

function searchProduit($searchValues){
    $pdo = getPDOConnect();

    $stmt = $pdo->prepare("SELECT
        `id`,
        `domaine`,
        `year`,
        `grape`,
        `country`,
        `region`,
        `description`, 
        `picture`

    FROM
        `products`
    WHERE `domaine`LIKE :search
    OR `year` LIKE :search
    OR `grape` LIKE :search
    OR `country` LIKE :search
    OR `region` LIKE :search
    GROUP BY `id`
    ORDER BY `create_time`
    DESC");
    $stmt->bindParam(':search',$searchValues);
    $res = $stmt->execute();
    if($res){
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}


//LIKES / DISLIKES
//1. verifier si cet utilisateur a déjà ajouté un like sur ce produit
function checkLike($id_utilisateur,$id_produit){
    $pdo = getPDOConnect();

    $stmt = $pdo->prepare("SELECT `id_product`  
    FROM product_like 
    WHERE `id_user`=:id_user AND `id_product`=:id_product");
    
    $stmt->bindParam(':id_user',$id_utilisateur);
    $stmt->bindParam(':id_product',$id_produit);
    $res = $stmt->execute();
    if($res){
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}
function checkDislike($id_utilisateur,$id_produit){
    $pdo = getPDOConnect();

    $stmt = $pdo->prepare("SELECT `id_product`  
    FROM product_dislike 
    WHERE `id_user`=:id_user AND `id_product`=:id_product");
    
    $stmt->bindParam(':id_user',$id_utilisateur);
    $stmt->bindParam(':id_product',$id_produit);
    $res = $stmt->execute();
    if($res){
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}
// Si pas encore
//2.1. Ajouter +1 like dans ce produit
function addLike($id_produit){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("UPDATE `products` 
    SET `likes`=`likes`+1
    WHERE `id`=:id");
    $stmt ->bindParam(':id',$id_produit);

    $res = $stmt->execute();
    if($res){
        return true;
    }else{
        return false;
    }
}
function addDisLike($id_produit){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("UPDATE `products` 
    SET `dislikes`=`dislikes`+1
    WHERE `id`=:id");
    $stmt ->bindParam(':id',$id_produit);

    $res = $stmt->execute();
    if($res){
        return true;
    }else{
        return false;
    }
}
//3.1. Mettre à jour des informations de l'utilisateur ainsi le du produit de like
function updateAddLikeInfor($id_produit,$id_utilisateur){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("INSERT INTO `product_like`(id_product, id_user) 
    VALUES (:id_product,:id_user)");
    $stmt -> bindParam(':id_product',$id_produit);
    $stmt -> bindParam(':id_user',$id_utilisateur);

    $res = $stmt->execute();

    if(!$res){
        return false;
    }
    return true;
}
function updateAddDislikeInfor($id_produit,$id_utilisateur){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("INSERT INTO `product_dislike`(id_product, id_user) 
    VALUES (:id_product,:id_user)");
    $stmt -> bindParam(':id_product',$id_produit);
    $stmt -> bindParam(':id_user',$id_utilisateur);

    $res = $stmt->execute();

    if(!$res){
        return false;
    }
    return true;
}


//Sinon déjà
//DELETE FROM `products` WHERE `id`=:id
//2.2. Reduire -1 like dans ce produit
function deleteLike($id_produit){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("UPDATE `products` 
    SET `likes`=`likes`-1
    WHERE `id`=:id");
    $stmt ->bindParam(':id',$id_produit);

    $res = $stmt->execute();
    if($res){
        return true;
    }else{
        return false;
    }
}
function deleteDisLike($id_produit){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("UPDATE `products` 
    SET `dislikes`=`dislikes`-1
    WHERE `id`=:id");
    $stmt ->bindParam(':id',$id_produit);

    $res = $stmt->execute();
    if($res){
        return true;
    }else{
        return false;
    }
}
//3.2. Mettre à jour des informations de l'utilisateur ainsi le du produit de like
function updateDeleteLikeInfor($id_produit,$id_utilisateur){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("DELETE FROM `product_like`
    WHERE `id_product`=:id_product AND `id_user`=:id_user");
    $stmt -> bindParam(':id_product',$id_produit);
    $stmt -> bindParam(':id_user',$id_utilisateur);

    $res = $stmt->execute();

    if(!$res){
        return false;
    }
    return true;
}
function updateDeleteDislikeInfor($id_produit,$id_utilisateur){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("DELETE FROM `product_dislike`
    WHERE `id_product`=:id_product AND `id_user`=:id_user");
    $stmt -> bindParam(':id_product',$id_produit);
    $stmt -> bindParam(':id_user',$id_utilisateur);

    $res = $stmt->execute();

    if(!$res){
        return false;
    }
    return true;
}


//PRODUCT COMMENTAIRE
//Affichier des commentaires
function getCommentaires($id_produit){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("SELECT 
        pc.commentaire,
        u.nom,
        u.prenom,
        pc.create_time
    FROM 
        product_commentaire pc
    JOIN users u ON pc.id_user = u.id
    WHERE  `id_product`= :id_product
    ORDER BY pc.create_time DESC");
    $stmt->bindParam(':id_product',$id_produit);
    $res= $stmt->execute();
    if($res){
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}

function addCommentaire($id_produit,$id_utilisateur,$commentaire){
    $pdo = getPDOConnect();
    $stmt = $pdo->prepare("INSERT INTO `product_commentaire`(id_product, id_user,commentaire) 
    VALUES (:id_product,:id_user,:commentaire)");
    $stmt -> bindParam(':id_product',$id_produit);
    $stmt -> bindParam(':id_user',$id_utilisateur);
    $stmt -> bindParam(':commentaire',$commentaire);

    $res = $stmt->execute();
    if(!$res){
        return false;
    }
    return true;
}

?>