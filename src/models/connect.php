<?php
try{
    $pdo = new PDO(
        'mysql:host=localhost;dbname=mycave; charset=utf8',
        'root',
        '',
        [  
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ]
    );
}catch(\PDOException $error){
    //S'il y a erreur PDO, nous l'affichons
    echo $error->getMessage();
}

?>