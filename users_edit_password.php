<?php
$pageTitle = "Modifier mon mot de passe";
require_once('src/controllers/users_edit_password.php');
session_start();

ob_start();
?>
  <div class="title">
    <h1 class="text-center pt-5 mb-5" id="utilisateur_modifi_password_h1"> MODIFIER MON MOT DE PASSE </h1>
  </div>
  <div class="container d-flex justify-content-center">
    
   <div class="utilisateur_modifi_box">
<?php if(isset($msgError)){  ?>
      <div class="my-3 text-center">
          <a href="#" class="alert alert-danger" style="text-decoration: none;"><?php echo $msgError ?></a>
      </div>
<?php }  ?>    
      <form action="users_edit_password.php?id_utilisateur=<?php echo $_SESSION['utilisateur']['id'] ?>" method="POST">
          <div class=" mb-4 text-center">
              <label for="enter_password_actuel">Mon de passe actuel * :</label>
              <input type="password" class="form-control mt-2 text-center" id="enter_password_actuel" name="enter_password_actuel" >
          </div>
          <div class=" mb-4 text-center">
                <label for="new_password">Nouveau mot de passe * :</label>
                <input type="password" class="form-control mt-2 text-center" id="new_password" name="new_password">
          </div>
          <div class=" mb-4 text-center">
                <label for="confirm_new_password">Confirmez le nouveau mot de passe * :</label>
                <input type="password" class="form-control mt-2 text-center" id="confirm_new_password" name="confirm_new_password">
          </div>
          
          <button type="submit" name="modifier" class="btn px-5 mt-3" style="position: relative;left: 50%; transform: translateX(-50%);"> MODIFIER </button>
      </form>

      
    </div>
  </div>





<?php
$content = ob_get_clean();
require_once('templates/layout.php');

?>