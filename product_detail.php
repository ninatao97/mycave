<?php
require_once('src/models/products.php');
require_once('./src/controllers/product_commentaire.php');
$id_produit = $_GET['id_produit'];
$produit = getDetailProduit($id_produit);

$pageTitle = "Detail de produit - ".$produit['domaine'];
 if(session_status() != PHP_SESSION_ACTIVE){
  session_start();
}
 ob_start();
?>
<div class="title">
        <h1 class="text-center pt-5 mb-5" id="produit_detail_h1"> DETAIL D'UN ELEMENT </h1>
</div>
<?php if(isset($msgError)){  ?>
  <a href="#" class="alert alert-danger" style="text-decoration: none;"><?php echo $msgError ?></a>
<?php }  ?>
<div class="container">
    <div class="card mt-3" style="max-width: 540px; overflow:hidden; margin:auto;">
    <!--ici le id_produit viens de index.php pour identifier cette page selon le id -->
      <form action="product_detail.php?id_produit=<?php echo $produit['id'] ?>" method="POST">
          <div class="img-box mt-5 mx-auto" style="width: 300px;height: 400px; ">
            <img src="./img/<?php echo $produit['picture'] ?>" style="display: block; margin: 0 auto;" alt="photo detail de produit">
          </div>
          <div class="card-body">
            <h5 class="card-title text-center mt-1 mb-3"><?php echo $produit['domaine'] ?></h5>
          <?php if($_SESSION['utilisateur']['poid'] >= 10){  ?>
            <div class="justify-content-center d-flex">
              <a href="./src/controllers/product_actions.php?t=1&id_produit=<?php echo $produit['id'] ?>" 
              class="btn btn-primary mx-3 px-5"><i class="far fa-thumbs-up"></i>(<?php echo $produit['likes'] ?>)</a>
              <a href="./src/controllers/product_actions.php?t=2&id_produit=<?php echo $produit['id'] ?>" 
              class="btn btn-primary mx-3 px-5"><i class="far fa-thumbs-down"></i>(<?php echo $produit['dislikes'] ?>)</a>
          </div>
          <?php } ?> 
            
            <ul class="list-group mt-3 list-group-flush">
              <li class="list-group-item text-center mx-auto">Pays : <?php echo $produit['country'] ?></li>
              <li class="list-group-item text-center mx-auto">Région : <?php echo $produit['region'] ?></li>
              <li class="list-group-item text-center mx-auto">Année :  <?php echo $produit['year'] ?></li>
              <li class="list-group-item text-center mx-auto">Grape : <?php echo $produit['grape'] ?></li>
            </ul>
            <h5 class="mt-5 text-center"> Description </h5>
            <p class="card-text text-center my-3 px-5"><?php echo $produit['description'] ?></p>
      <?php if($_SESSION['utilisateur']['poid']>10){  ?>    
            <div class="d-flex justify-content-center my-5">
              <a href="./product_edit.php?id_produit=<?php echo $produit['id'] ?>" class="btn btn-primary mx-3 px-5"> Modifier </a>
              <a href="./src/controllers/product_delete.php?id_produit=<?php echo $produit['id'] ?>" class="btn btn-danger mx-3 px-5"> Supprimer </a>
           </div>    
      <?php } ?>  
            <p class="card-text text-center mt-3">Date de mise à jour : <?php echo $produit['update_time'] ?></p>
            <div class="commentaire_box my-5">
              <form action="product_commentaire.php?id_produit=<?php echo $produit['id'] ?>" method="POST">
                  <div class="mt-3 text-center">
                      <label for="description">
                        Évaluer ce produit<br>
                        Partagez votre opinion avec les autres clients
                      </label>
                      <textarea class="form-control my-2 m-auto" id="commentaire" name="commentaire" style="height: 200px; width: 400px; resize:none;"></textarea>
                  </div>
                  <div class="text-center">
                      <button type="submit" name="valider" class="btn btn-primary mt-5 px-5" > VALIDER </button>
                  </div>
              </form>
              <?php foreach($commentaires as $commentaire){ ?>
                <div class="commentaire text-center">
                  <p><?php echo $commentaire['commentaire']; ?></p>
                  <span><?php echo $commentaire['nom']; ?></span>
                  <span><?php echo $commentaire['prenom']; ?></span>
                  <p><?php echo $commentaire['create_time']; ?></p>
                </div>
              <?php } ?>
            </div>
          </div>
      </form>
    </div>
</div>

<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>