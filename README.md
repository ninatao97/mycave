Projet myCave :
- un site catalogue, réalisé en PHP

* Gestion de Product : CRUD
1. Détails de produit (pour des comptes connectés)
2. Création d'un produit (pour compte professionnel && administrateur)
3. Modification de produit
4. Suppression de produit
5. Evaluer un produit : 
    * Like || Dislike
    * Commentaires
6. Recherche d'un produit

* Gestion d'User : 
1. Inscrption
    * Compte client pour le public
    * Compte professionnel créé par l'administrateur
2. Authentification 
3. Autorisation selon les rôles  --- client && professionnel && administrateur
    * Modification de compte (pour tous les comptes):
        + Modification d'informations personnelles 
            - Adresse Email
            - Nom 
            - Prénom
        + Modification de mot de passe
            - Vérification de MDP actuel
            - Nouveau MDP !== MDP actuel
            - Nouveau MDP === Confirmation de nouveau MDP
    * Gestion de comptes pour l'administrateur :
        + Création de compte professionnel ou compte client
        + Détails d'un compte
        + Modifier d'un compte
        + Supprimer de comptr
        + Filtres de comptes différents


A améliorer :
    1. POO
    2. Evaluer via site e-commerce 


    
