<?php
$pageTitle = "Résultats de recherche";
require_once('./src/controllers/users_search.php');
ob_start();
?>
<div style="max-width: 992px; position: relative; left: 50%; transform: translateX(-50%);">
    <div class="title">
        <h1 class="text-center pt-5 mb-5" id="utilisateur_search_h1">Résultats de recherche</h1>
    </div>
<?php if(isset($msgError)){  ?>
    <div>
        <h2 class="text-center my-5" style="color: #DACECE;"><?php echo $msgError; ?></h2>
    </div>
<?php }  ?>
    <div class="container-fluid d-flex justify-content-center">
        <form action="./users_search.php" class="utilisateur_searchForm" method="GET">
            <select class="mx-4 px-3 py-1" name="search" id="search_utilisateur_type">
                <option value="" selected disabled="disabled"> Filtrer par type de compte  </option>
                <option value="visiteur"> VISITEUR </option>
                <option value="professionnel"> PROFESSIONNEL </option>
            </select>
            <div class="search_button">
                <button class="btn btn-outline-light mx-3" type="submit"> Search </button>        
                <a class="btn btn-outline-light mx-3" href="./users_list.php"> Tous les types </a>        
            </div>
        </form>
    </div>
    <div class="container mt-5 px-5">
<?php if(!isset($msgError)){  
 foreach($utilisateurs as $utilisateur){ ?>
    <div class="row mb-3">
            <div class="col-sm-6 col-md-8 bg-white p-3">
                <h5><?php echo $utilisateur['nom'] ?></h5>
                <p><?php echo $utilisateur['mail'] ?></p>
                <p>Type de compte : <?php echo $utilisateur['name'] ?></p>
            </div>
            <div class="col-sm-6 col-md-4 bg-secondary p-3">
                <div class="button_box">
                    <a href="./users_detail.php?id_utilisateur=<?php echo $utilisateur['id'] ?>" class="btn btn-primary mx-2 px-5 text-center"><i class="bi bi-eye"></i> Détails </a>
                    <a href="src/controllers/users_delete.php?id_utilisateur=<?php echo $utilisateur['id'] ?>" class="btn btn-danger mx-2 px-5 text-center"><i class="bi bi-trash2"></i> Supprimer</a>
                </div>    
            </div>
    </div>
<?php } 
} ?>
    </div>
</div>

<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>