<?php
 $pageTitle = "Login";
 require_once('src/controllers/login.php');
 ob_start();
?>

<div class="title">
    <h1 class="text-center  pt-5 mb-lg-5 mb-md-2">Identification</h1>
</div>
 <div class="container d-flex justify-content-center inscription_list text-center pt-2 pb-5">
            <?php if(isset($msgError)){  ?>
            <div class="my-5">
                <a href="#" class="alert alert-danger" style="text-decoration: none;"><?php echo $msgError ?></a>
            </div>
            <?php }  ?>
            <div class="inscription_box mt-lg-3 mb-lg-3 mt-md-1 mb-md-1 mx-3 px-3 py-5"> 
                <h3 class="text-center mb-5">DEJA CLIENT</h3> 
                <p class="text-center">
                    connectez-vous <br>
                    pour accéder à votre compte
                </p>
                <form action="login.php" style="position: relative;" method="POST">
                    <div class="input_boxs">
                        <input class="my-2 py-1 px-5 text-center" style="border-radius: 18px; outline: none; border: 2px solid #DACECE ;" type="text" name="mail" id="mail" placeholder="Mail *">
                        <input class="my-2 py-1 px-5 text-center" style="border-radius: 18px; outline: none; border: 2px solid #DACECE ;" type="password" name="password" id="password" placeholder="Mot de passe *">
                    </div>
                    <a href="#" id="mdp_oublie">mot de passe oublié?</a>
                    <input class="btn mt-3" type="submit" id="connect" name="connect" value="ME CONNECTER">
                </form>
            </div>
            <div class="inscription_box mt-lg-3 mb-lg-3 mt-md-1 mb-md-1 mx-5 p-5"> 
                <h3 class="text-center mb-5">NOUVEAU CLIENT</h3> 
                <p class="text-start text-des">Tisser un lien unique avec myCAVE en créant <br> 
                    un compte. Prolongez ainsi l'expérience en recevant <br> 
                    un contenu personnalisé, l'accès à des <br> 
                    management de votre stock
                </p>
                <div>
                    <a class="btn mt-4" id="create_compte_button" href="./inscription.php">CREER UN COMPTE</a>
                </div>
            </div>
 </div>

<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>