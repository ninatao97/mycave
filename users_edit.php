<?php
$pageTitle = "Modification mon compte";
session_start();
ob_start();
?>
<div class="title">
    <h1 class="text-center pt-5 mb-5" id="utilisateur_modifi_h1"> MODIFICATION DE MON COMPTE </h1>
</div>
<div class="container-fluid container_modifi mt-5">
    <div class="card mb-5" style="max-width: 540px; overflow:hidden; margin:auto;">
        <div class="card-body py-4">
            <h5 class="card-title text-center">Mes informations personnelles</h5>
            <div class="d-flex justify-content-center mt-3">
                <a href="./users_edit_infor.php?id_utilisateur=<?php echo $_SESSION['utilisateur']['id'] ?>" class="btn px-4">Modifier</a>
            </div>   
        </div>
    </div>
    <div class="card mt-5" style="max-width: 540px; overflow:hidden; margin:auto;">
        <div class="card-body py-4">
            <h5 class="card-title text-center">Mot de passe</h5>
            <div class="d-flex justify-content-center mt-3">
                <a href="./users_edit_password.php?id_utilisateur=<?php echo $_SESSION['utilisateur']['id'] ?>" class="btn px-4">Modifier</a>
            </div>
        </div>
    </div>
</div>
    

<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>