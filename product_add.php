<?php
require_once('src/controllers/product_add.php');
 $pageTitle = "Ajouter un nouveau produit";
 ob_start();
?>
<div class="title">
        <h1 class="text-center pt-5 mb-5" id="produit_add_h1"> AJOUTER UN ELEMENT </h1>
</div>
<div class="container d-flex justify-content-center">        
      <div class="produit_add_box">
<?php if(isset($msgError)){  ?>
            <div class="my-5 text-center">
                <a href="#" class="alert alert-danger" style="text-decoration: none;"><?php echo $msgError ?></a>
            </div>
<?php }  ?>    
          <form action="product_add.php" method="POST" enctype="multipart/form-data" class="d-flex flex-column justify-content-center">
            <div class="mb-3 text-center">
                  <label for="domaine">DOMAINE * :</label>
                  <input type="text" class="form-control my-2 text-center" id="domaine" name="domaine">
            </div>
            <div class="mb-3 text-center">
                  <label for="country">Pays * :</label>
                  <input type="text" class="form-control my-2 text-center" id="country" name="country">
            </div>
            <div class="mb-3 text-center">
                  <label for="region">Région * :</label>
                  <input type="text" class="form-control my-2 text-center" id="region" name="region">
            </div>
            <div class="mb-3 text-center">
                  <label for="year">Année * :</label>
                  <input type="text" class="form-control my-2 text-center" id="year" name="year">
            </div>
            <div class="mb-3 text-center">
                  <label for="grape">Grape * :</label>
                  <input type="text" class="form-control my-2 text-center" id="grape" name="grape">
            </div>
            <div class="mb-3 text-center">
                  <label for="picture" class="form-label"> Séléctionner un ficher *</label>
                  <input class="form-control text-center" type="file" id="picture" name="picture">
            </div>
            <div class="mt-3 text-center">
                  <label for="description">Description</label>
                  <textarea class="form-control my-2" id="description" name="description" style="height: 200px; width: 500px; resize:none;"></textarea>
            </div>
            <div class="mt-3 text-center">
                  <button type="submit" name="creer" class="btn mt-5 px-5" > CREER </button>
            </div>

          </form>
      </div>
</div>
<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>