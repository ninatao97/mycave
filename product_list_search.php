<?php
    $pageTitle = "Résultats de recherche";
    require_once('./src/controllers/product_search.php');
    ob_start();

?>
<div class="title">
          <h1 class="text-center pt-5 mb-5" id="utilisateur_list_search_h1">Résultats de recherche</h1>
</div>
<div class="container-fluid d-flex justify-content-center my-3">
          <form class="d-flex my-3" action="./produit_list_search.php" method="GET">
            <input class="form-control me-2" type="search" name="search" placeholder="RECHERCHE" aria-label="Search">
            <button class="btn btn-outline-light mx-3" type="submit"> Search </button>
            <a class="btn btn-outline-light mx-3" href="./page_all_products.php"> Tous les produits </a>             
          </form>
</div>
<div class="container">
        <?php foreach($produits as $produit){ ?>
          <div class="card mb-3" style="max-width: 540px; overflow:hidden; margin:auto;">
            <div class="row g-0">
              <div class="col-md-4" style="margin: auto;">
                <img class="j-center" style="position: relative; left: 50%; transform: translateX(-50%);" src="./img/<?php echo $produit['picture'] ?>" alt="Photo du produit">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title"><?php echo $produit['domaine'] ?></h5>
                  <ul class="list-group list-group-flush">
                      <li class="list-group-item"> <?php echo $produit['country'] ?></li>
                      <li class="list-group-item"> <?php echo $produit['year'] ?></li>
                      <li class="list-group-item"> <?php echo $produit['grape'] ?></li>
                  </ul>
                  <div class="d-flex justify-content-center mb-3 mt-5">
                        <a href="./product_detail.php?id_produit=<?php echo $produit['id'] ?>" class="btn btn-primary mx-2"> DETAIL </a>
                        <a href="./product_edit.php?id_produit=<?php echo $produit['id'] ?>" class="btn btn-primary mx-2"> MODIFIER </a>
                        <a href="src/controllers/product_delete.php?id_produit=<?php echo $produit['id'] ?>" class="btn btn-primary mx-2"> SUPPRIMER </a>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          <?php } ?>
</div>




<?php
$content = ob_get_clean();
require_once('templates/layout.php');

?>