<?php
$pageTitle = "Details de l'utilisateur";
require_once('src/controllers/users_detail.php');
ob_start();
?>
  <div class="title">
        <h1 class="text-center pt-5 mb-5" id="utilisateur_detail_h1"> DETAIL DE L'UTILISATEUR </h1>
  </div>
  <?php if(isset($msgError)){  ?>
    <a href="#" class="alert alert-danger" style="text-decoration: none;"><?php echo $msgError ?></a>
  <?php }  ?>
  <div class="container">
    <div class="card mt-5" style="max-width: 540px; margin:auto;">
    <!--ici le id_produit viens de index.php pour identifier cette page selon le id -->
        <div class="card-body">
          <ul class="list-group mt-5 list-group-flush">
            <li class="list-group-item text-center mx-auto">Nom : <?php echo $utilisateur['nom'] ?></li>
            <li class="list-group-item text-center mx-auto">Prénom : <?php echo $utilisateur['prenom'] ?></li>
            <li class="list-group-item text-center mx-auto">Mail : <?php echo $utilisateur['mail'] ?></li>
            <li class="list-group-item text-center mx-auto">Type de compte : <?php echo $utilisateur['name'] ?></li>
            <li class="list-group-item text-center mx-auto">Date mis à jour : <?php echo $utilisateur['update_time'] ?></li>
          </ul>
        </div>
        <div class="d-flex justify-content-center my-5">
          <a href="src/controllers/users_delete.php?id_utilisateur=<?php echo $utilisateur['id'] ?>" class="btn btn-danger px-5"> Supprimer </a>
        </div>
    </div>
  </div>



<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>