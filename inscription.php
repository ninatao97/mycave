<?php

require_once('src/controllers/inscription.php');
 $pageTitle = "Inscription";
 ob_start();
?>

<div class="title">
    <h1 class="text-center pt-5 mb-5" id="inscription_h1">CREER MON COMPTE</h1>
</div>
<div class="container mt-5 d-flex justify-content-center">
    <div class="inscription_form_box mt-5">
<?php if(isset($msgError)){  ?>
        <div class="my-3 text-center">
            <a href="#" class="alert alert-danger" style="text-decoration: none;"><?php echo $msgError ?></a>
        </div>
<?php }  ?>
        <form action="inscription.php" method="POST">
        <?php if(isset($_SESSION['utilisateur']) && $_SESSION['utilisateur']['poid'] > 50){ ?>
            <div class="mb-4 text-center">
                <label for="id_role">Choisissez un type de compte * :</label>
                <select name="id_role" id="id_role" class="px-3">
                    <option value="1"> Client </option>
                    <option value="2"> Professionnel  </option>
                </select>
            </div> 
        <?php }else{ ?>
            <input type="hidden" name="id_role" value="1">
        <?php } ?>  

            <div class="mb-4 text-center">
                <label for="name">Nom * :</label>
                <input type="text" class="form-control mt-2 text-center" id="nom" name="nom">
            </div>
            <div class="mb-4 text-center">
                  <label for="web_site">Prénom * :</label>
                  <input type="text" class="form-control mt-2 text-center" id="prenom" name="prenom">
            </div>
            <div class="mb-4 text-center">
                  <label for="web_site">Mail * :</label>
                  <input type="text" class="form-control mt-2 text-center" id="mail" name="mail">
            </div>
            <div class="mb-4 text-center">
                  <label for="web_site">Mot de passe * :</label>
                  <input type="password" class="form-control mt-2 text-center" id="password" name="password">
            </div>             
           
            <div class="condition mt-2 text-center" >
                <input type="checkbox" name="accept" id="accept">
                <label for="accept">J'accepte tous les conditions</label>
            </div>
            
            <div class="mt-3 text-center">
                <button type="submit" name="valider" class="btn px-5 mt-3" id="inscription_btn" style="background-color: #885c7e;color: #DACECE;outline: none;">VALIDER</button>
            </div>
        </form>
    </div>
</div>


<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>
