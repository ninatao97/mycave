<?php
//ficher acl.php c'est pour gestioner le droit d'accéder dans les pages

/*Etape 1: si utilisateur n'est pas connecté(utiliser if(session_status() != PHP_SESSION_ACTIVE) pour voir s'il est connecté ), 
il peut aller dans le page login pour ajouter 
ses infors dans le tableau [utilisateur] avec la méthode $_SESSION
*/
if(session_status() != PHP_SESSION_ACTIVE){
  session_start();
}
require_once('src/controllers/acl.php');
$pageTitle = $pageTitle ?? '';


?>
<!DOCTYPE html>
<html lang="en">
<?php require_once('templates/head.php');  ?>
<body style="background-color: #88002D; ">
<?php require_once('templates/header.php');  ?>
  <main class=" container-fluid p-0  mb-5" style="width: 100%;">
<?php
    if(isset($_SESSION['msgReussite'])){  ?>
      <div class="text-center text-light"> <?php echo $_SESSION['msgReussite'] ?> , <?php echo $_SESSION['utilisateur']['prenom']  ?> </div>
<?php  unset($_SESSION['msgReussite']);
    }
    echo $content;
?>
  </main>
<?php require_once('templates/footer.php');  ?>
</body>
</html>