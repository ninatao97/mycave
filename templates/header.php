<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #3e0018;">
    <div class="container-fluid">
      <a class="navbar-brand text-white" href="./index.php">myCAVE</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
        <ul class="navbar-nav mb-2 mb-lg-0 ms-auto">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="./page_all_products.php">Nos produits</a>
          </li>
<?php if(isset($_SESSION['utilisateur'])){ ?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Administration
            </a>
            <ul class="dropdown-menu navbar-dark bg-dark" aria-labelledby="navbarDropdown">
          <?php if($_SESSION['utilisateur']['poid'] > 10){ ?>
              <li class="text-white p-2">Produits</li>
              <li><a class="dropdown-item text-muted" href="./product_list.php">Gestion des produits</a></li>
              <li><a class="dropdown-item text-muted" href="./product_add.php">Ajouter un produit</a></li>
          <?php } ?>
              <li class="text-white p-2">Utilisateur</li>
          <?php if($_SESSION['utilisateur']['poid'] >= 10){ ?>
            <li><a class="dropdown-item text-muted" href="users_edit.php">Gestion de mon compte</a></li>
          <?php } ?>
          <?php if($_SESSION['utilisateur']['poid'] > 50){ ?>
              <li><a class="dropdown-item text-muted" href="./inscription.php">Ajouter un nouveau utilisateurs</a></li>
              <li><a class="dropdown-item text-muted" href="users_list.php">Gestion des utilisateurs</a></li>
          <?php } ?>
            </ul>
          </li>
<?php } ?>
        <?php if(!isset($_SESSION['utilisateur'])){ ?>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="login.php">Me Connecter</a>
          </li>
        <?php }else{ ?>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="src/controllers/logout.php">Déconnecter</a>
          </li>
        <?php } ?>
        </ul>
      </div>
    </div>
  </nav>