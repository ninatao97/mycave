<?php
    $pageTitle = "Résultats de recherche";
    require_once('./src/controllers/product_search.php');
    ob_start();
    

?>
<div class="title">
    <h1 class="text-center pt-5 mb-5" id="produit_search_h1">Résultats de recherche</h1>
</div>
<?php if(isset($msgError)){  ?>
<div>
      <h2 class="text-center my-5" style="color: #DACECE;"><?php echo $msgError; ?></h2>
</div>
<?php }  ?>
<div class="container-fluid d-flex justify-content-center my-3">
    <form class="d-flex my-3" action="./product_search.php" method="GET">
      <input class="form-control me-2" type="search" name="search" placeholder="RECHERCHE" aria-label="Search">
      <button class="btn btn-outline-light mx-3" type="submit"> Search </button>          
      <a class="btn btn-outline-light mx-3" href="./page_all_products.php"> EFFACER </a>                       
    </form>
</div>
<div class="container" >
<?php if(!isset($msgError)){ 
foreach($produits as $produit){ ?>
    <div class="card mb-5 p-3 card_produit" style="max-width: 540px; overflow:hidden; margin:auto;">
              <div class="row">
                <div class="col-md-4" style="margin: auto;">
                  <img class="j-center" style="position: relative; left: 50%; transform: translateX(-50%);" src="./img/<?php echo $produit['picture'] ?>" alt="Photo du produit" style="margin: auto;">
                </div>
                <div class="col-md-8 p-3">
                  <div class="card-body produit_box">
                    <h5 class="card-title text-center"><?php echo $produit['domaine'] ?></h5>
                    <ul class="list-group mt-3 list-group-flush">
                        <li class="list-group-item text-center">Pays : <?php echo $produit['country'] ?></li>
                        <li class="list-group-item text-center">Année :  <?php echo $produit['year'] ?></li>
                        <li class="list-group-item text-center">Grape : <?php echo $produit['grape'] ?></li>
                    </ul>
                    <div class="d-flex justify-content-center mb-3 mt-5">
                        <a href="./product_detail.php?id_produit=<?php echo $produit['id'] ?>" class="btn"> PLUS D'INFORMATION </a>
                     </div>                   
                  </div>
                </div>
              </div>
              
    </div>
<?php } 
} ?>
  </div>




<?php
$content = ob_get_clean();
require_once('templates/layout.php');

?>