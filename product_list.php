<?php
require_once('src/models/products.php');
 $pageTitle = "Liste de produits"; 
 $produits = getProduitsList();
 ob_start();

?>
<div class="title">
          <h1 class="text-center pt-5 mb-5">NOTRE CAVE</h1>
</div>
<div class="container-fluid d-flex justify-content-center">
          <a href="./product_add.php" class="btn btn_ajouter mx-2"> + AJOUTER UN ELEMENT </a>
</div>
<div class="container-fluid d-flex justify-content-center">
          <form class="d-flex my-3" action="./produit_list_search.php" method="GET">
            <input class="form-control me-2" type="search" name="search" placeholder="RECHERCHE" aria-label="Search">
            <button class="btn btn-outline-light mx-3" type="submit"> Search </button>
          </form>
</div>
<div class="container mt-5">
          <?php foreach($produits as $produit){ ?>
          <div class="card my-5 p-3" style="max-width: 540px;overflow:hidden; margin:auto;">
            <div class="row">
              <div class="col-md-4" style="margin: auto;">
                <img class="j-center" style="position: relative; left: 50%; transform: translateX(-50%);" src="./img/<?php echo $produit['picture'] ?>" alt="...">
              </div>
              <div class="col-md-8 p-3">
                <div class="card-body text-center produit_list_box">
                  <h4 class="card-title"><?php echo $produit['domaine'] ?></h4>
                  <div class="justify-content-center d-flex">
                    <span class="mx-3"><i class="far fa-thumbs-up"></i>(<?php echo $produit['likes'] ?>)</a>
                    <span class="mx-3"><i class="far fa-thumbs-down"></i>(<?php echo $produit['dislikes'] ?>)</a>
                  </div>
                  <h5><?php echo $produit['country'] ?></h5>
                  <h5><?php echo $produit['year'] ?></h5> 
                  <div class="d-flex flex-column mt-3">
                      <a href="./product_detail.php?id_produit=<?php echo $produit['id'] ?>" class="btn btn_detail my-2" style="width: 50%; margin: 0 auto;"> DETAIL </a>
                      <a href="./product_edit.php?id_produit=<?php echo $produit['id'] ?>" class="btn btn-primary my-2" style="width: 50%; margin: 0 auto;"> MODIFIER </a>
                      <a href="src/controllers/product_delete.php?id_produit=<?php echo $produit['id'] ?>" class="btn btn-danger my-2" style="width: 50%; margin: 0 auto;"> SUPPRIMER </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
</div>
<?php
$content = ob_get_clean();
require_once('templates/layout.php');
?>